#version 150

#define PI 3.14159265f
#define EPS 0.001f

// Object types
#define SPHERE 0
#define PLANE  1

// Amount of objects in the scene
#define NUM_OBJECTS 3

// In and outputs
uniform vec2 resolution;
uniform vec3 cameraPos;
uniform vec2 cameraRot;
in vec2 pos;
out vec4 outColor;

// TODO: move to main
float ratio = resolution.x / resolution.y;
float fovX = radians(45.0f);
float fovY = fovX / ratio;

// Scene definitions
vec3 light = vec3(5.0f, 10.0f, 10.0f);
vec3 lightCol = vec3(1.0f);

vec3 skyColor = vec3(0.3f, 0.6f, 0.8f);

// Structs
struct object
{
	int type;
	vec3 pos;
	vec3 color;
	float reflFac;
	float specFac;

	// in case of sphere
	float r;
};

struct intersection
{
	object o;
	float d;
	vec3 pos;
	vec3 norm;
};

// TODO: make different axes work too
intersection intersectFloor(vec3 orig, vec3 dir, object p)
{
	intersection is;
	is.o = p;
	is.d = (p.pos.y - orig.y) / dir.y;

	if (is.d < 0.0f)
		return is;

	is.pos = is.d * dir + orig;
	is.norm = vec3(0.0f, 1.0f, 0.0f);

	return is;
}


intersection intersectSphere(vec3 orig, vec3 dir, object s)
{
	intersection is;
	is.o = s;

	vec3 rc = orig - s.pos;
	float drc = dot(dir, rc);
	float d2 = dot(dir, dir);
	float sq = drc * drc - d2 * (dot(rc, rc) - (s.r * s.r));

	if (sq < 0.0f)
	{
		is.d = -1.0f;
		return is;
	}

	sq = sqrt(sq);
	float t1 = (-drc + sq) / d2;
	float t2 = (-drc - sq) / d2;

	is.d = min(t1, t2);
	is.pos = is.d * dir + orig;
	is.norm = (is.pos - s.pos) / s.r;

	return is;
}

intersection intersectObject(vec3 orig, vec3 dir, object o)
{
	intersection is;

	if (o.type == SPHERE)
		is = intersectSphere(orig, dir, o);

	else if (o.type == PLANE)
		is = intersectFloor(orig, dir, o);

	return is;
}

vec3 intersectScene(vec3 orig, vec3 dir, int maxRec)
{
	object objects[NUM_OBJECTS];

	// Floor
	objects[0].type = PLANE;
	objects[0].pos = vec3(0.0f); // does nothing
	objects[0].color = vec3(0.5f, 0.5f, 0.0f);
	objects[0].reflFac = 0.3f;
	objects[0].specFac = 0.2f;

	// Sphere
	objects[1].type = SPHERE;
	objects[1].pos = vec3(-2.0f, 1.5f, -4.0f);
	objects[1].color = vec3(0.8f, 0.0f, 0.0f);
	objects[1].reflFac = 0.8f;
	objects[1].specFac = 0.5f;
	objects[1].r = 1.0f;

	// Sphere
	objects[2].type = SPHERE;
	objects[2].pos = vec3(2.0f, 1.5f, -4.0f);
	objects[2].color = vec3(0.0f, 0.0f, 0.8f);
	objects[2].reflFac = 0.8f;
	objects[2].specFac = 0.5f;
	objects[2].r = 1.0f;

	// Accumulative output color
	vec3 outCol = vec3(0.0f);

	float parentReflFac = 1.0f;
	int shadow = 0;
	for (int recLevel = 0; recLevel < maxRec; ++recLevel)
	{
		// Try to intersect all objects
		intersection closest, last;
		int closestI = -1;
		closest.d = 9999.0f;

		for (int i = 0; i < NUM_OBJECTS; ++i)
		{
			last = intersectObject(orig, dir, objects[i]);

			if (last.d > 0.0f && last.d < closest.d)
			{
				closest = last;
				closestI = i;
			}
		}

		// If some object was hit
		if (closestI != -1)
		{
			// Trace to light through all other objects
			intersection lightIs;
			for (int i = 0; i < NUM_OBJECTS; ++i)
			{
				// No point in tracing through itself or planes
				if (i == closestI || objects[i].type == PLANE)
					continue;

				// Trace from intersection point to light
				// TODO: multiple lights
				lightIs = intersectObject(closest.pos, light - closest.pos, objects[i]);

				// Something was hit, we're in shadow
				if (lightIs.d > 0.0f)
				{
					// Paint pixel black
					closest.o.color = vec3(0.0f);
					shadow = 1;
					break;
				}
			}
			
		}

		// No need for light calculations when we hit nothing
		if (closestI == -1 || shadow == 1)
			break;

		// Vector from intersection point to light
		vec3 lightVec = normalize(light - closest.pos);

		// Diffuse factor:
		// Dot product of surface normal and vector to light
		float difFac = clamp(dot(closest.norm, lightVec), 0.0f, 1.0f);

		// Specular factor:
		float dotp = dot(dir, (lightVec - (2.0f * dot(lightVec, closest.norm) * closest.norm)));
		float specFac = clamp(pow(dotp, 20) * closest.o.specFac, 0.0f, 1.0f);

		// Total output color
		outCol += parentReflFac * (closest.o.color * difFac + lightCol * specFac);

		// Reflect, set new origin and direction
		// Only if there's another level and if the object is reflective
		if (recLevel + 1 < maxRec && closest.o.reflFac > 0.0f + EPS)
		{
			vec3 reflDir = normalize(dir - 2.0f * dot(dir, closest.norm) * closest.norm);
			orig = closest.pos + reflDir * EPS;
			dir = reflDir;

			parentReflFac *= closest.o.reflFac;
		}
		else
		{
			break;
		}
	}

	return outCol;
}

mat4 rotationMatrix(vec3 axis, float angle)
{
	axis = normalize(axis);
	float s = sin(angle);
	float c = cos(angle);
	float oc = 1.0 - c;
	return mat4(
		oc * axis.x * axis.x + c, oc * axis.x * axis.y - axis.z * s, oc * axis.z * axis.x + axis.y * s, 0.0,
		oc * axis.x * axis.y + axis.z * s, oc * axis.y * axis.y + c, oc * axis.y * axis.z - axis.x * s, 0.0,
		oc * axis.z * axis.x - axis.y * s, oc * axis.y * axis.z + axis.x * s, oc * axis.z * axis.z + c, 0.0,
		0.0, 0.0, 0.0, 1.0
	);
}

void main()
{
	vec3 dir;

	dir.x += pos.x * tan(fovX);
	dir.y += pos.y * tan(fovY);
	dir.z = -1;

	dir = normalize(dir);

	float yaw = cameraRot.y;
	float pitch = -cameraRot.x;

	vec4 dir4 = vec4(dir, 1.0f) * rotationMatrix(vec3(1.0f, 0.0f, 0.0f), yaw) * rotationMatrix(vec3(0.0f, 1.0f, 0.0f), pitch);
	dir = dir4.xyz;

	// Calculate intersection point
	vec3 col = intersectScene(cameraPos, dir, 5);

    outColor = vec4(col, 1.0);
}