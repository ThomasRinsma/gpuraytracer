#include <iostream>
#include <fstream>
#include <string>
#include <vector>

#include <GL/glew.h>
#include <GL/glfw.h>

#define WIN_X 800
#define WIN_Y 600

using namespace std;

string file_get_contents(const char *filename)
{
	ifstream in(filename, ios::in | ios::binary);
	if (in)
	{
		string contents;
		in.seekg(0, ios::end);
		contents.resize(in.tellg());
		in.seekg(0, ios::beg);
		in.read(&contents[0], contents.size());
		in.close();

		return contents;
	}

	throw(1);
}

GLuint shaderProgram;
GLuint vertexShader;
GLuint fragmentShader;
string tmpString;
bool firstLoad = true;

bool getCompileErrors(GLuint shader, string const &shaderType)
{
	GLint isCompiled = 0;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &isCompiled);
	if (isCompiled == GL_FALSE)
	{
		GLint maxLength = 0;
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &maxLength);
	
		vector<char> errorLog(maxLength);
		glGetShaderInfoLog(shader, maxLength, &maxLength, &errorLog[0]);

		cout << "Error compiling " << shaderType << " shader:" << endl;
		cout << string(errorLog.data()) << endl;

		glDeleteShader(shader);
		return false;
	}

	return true;
}

bool loadShaders()
{
	if (!firstLoad)
	{
		glDeleteProgram(shaderProgram);
		glDeleteShader(fragmentShader);
		glDeleteShader(vertexShader);
	}

	vertexShader = glCreateShader(GL_VERTEX_SHADER);
	tmpString = file_get_contents("vertex.shader");
	const char *vertexSource = tmpString.c_str();
	glShaderSource(vertexShader, 1, &vertexSource, NULL);
	glCompileShader(vertexShader);

	if (not getCompileErrors(vertexShader, "vertex"))
		return false;

	fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	tmpString = file_get_contents("fragment.shader");
	const char *fragmentSource = tmpString.c_str();
	glShaderSource(fragmentShader, 1, &fragmentSource, NULL);
	glCompileShader(fragmentShader);

	if (not getCompileErrors(fragmentShader, "fragment"))
		return false;


	// Create the shader program
	shaderProgram = glCreateProgram();
	glAttachShader(shaderProgram, vertexShader);
	glAttachShader(shaderProgram, fragmentShader);

	glBindFragDataLocation(shaderProgram, 0, "outColor");
	glLinkProgram(shaderProgram);
	glUseProgram(shaderProgram);

	// Set resolution uniform
	GLuint resUniform = glGetUniformLocation(shaderProgram, "resolution");
	glUniform2f(resUniform, WIN_X, WIN_Y);

	firstLoad = false;

	return true;
}

int main( void )
{
	int forward, right, up;
	int turnUp, turnRight;
	float x = 0.0f, y = 0.0f, z = 0.0f;
	float pitch = 0.0f, yaw = 0.0f;
	float moveSpeed = 1.0f;
	float turnSpeed = 1.0f;

	// Initialise GLFW
	glfwInit();

	// Context parameters
	glfwOpenWindowHint(GLFW_FSAA_SAMPLES, 4);
	glfwOpenWindowHint(GLFW_OPENGL_VERSION_MAJOR, 3);
	glfwOpenWindowHint(GLFW_OPENGL_VERSION_MINOR, 3);
	glfwOpenWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	// Open a window and create its OpenGL context
	glfwOpenWindow(WIN_X, WIN_Y, 0, 0, 0, 0, 32, 0, GLFW_WINDOW);
	glfwSetWindowTitle("GPU Raytracer Test");

	// Initialize GLEW
	glewExperimental = GL_TRUE;
	glewInit();


	// This somehow fixes catching the escape key (?)
	glfwEnable(GLFW_STICKY_KEYS);

	// This doesn't matter
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	// Create Vertex Array Object
	GLuint vao;
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	// Create a Vertex Buffer Object and copy the vertex data to it
	GLuint vbo;
	glGenBuffers(1, &vbo);

	GLfloat vertices[] = {
		-1.0f, -1.0f,
		-1.0f, 1.0f,
		1.0f, -1.0f,

		1.0f, -1.0f,
		1.0f, 1.0f,
		-1.0f, 1.0f,
	};

	// Upload vertex data
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	// Load shaders
	if (not loadShaders())
		return 0;

	// Specify the layout of the vertex data
	GLint posAttrib = glGetAttribLocation(shaderProgram, "position");
	glEnableVertexAttribArray(posAttrib);
	glVertexAttribPointer(posAttrib, 2, GL_FLOAT, GL_FALSE, 0, 0);

	bool wasError = false;
	do
	{
		// Reload shaders every now and then
		if (glfwGetTime() >= 1.0f)
		{
			wasError = not loadShaders();
			glfwSetTime(0.0f);
		}

		// Check for input
		forward = (glfwGetKey('W') == GLFW_PRESS)
		        + (glfwGetKey('S') == GLFW_PRESS) * -1;

		right = (glfwGetKey('D') == GLFW_PRESS)
		      + (glfwGetKey('A') == GLFW_PRESS) * -1;

		up = (glfwGetKey(' ') == GLFW_PRESS)
		   + (glfwGetKey('C') == GLFW_PRESS) * -1;

		turnRight = (glfwGetKey(GLFW_KEY_RIGHT) == GLFW_PRESS)
		      + (glfwGetKey(GLFW_KEY_LEFT) == GLFW_PRESS) * -1;

		turnUp = (glfwGetKey(GLFW_KEY_UP) == GLFW_PRESS)
		   + (glfwGetKey(GLFW_KEY_DOWN) == GLFW_PRESS) * -1;

		// Update coordinates and rotation
		float dt = glfwGetTime();
		x += right * moveSpeed * dt;
		y += up * moveSpeed * dt;
		z += -forward * moveSpeed * dt;

		yaw += turnRight * turnSpeed * dt;
		pitch += turnUp * turnSpeed * dt;

		// Update uniforms
		GLuint cameraPosUniform = glGetUniformLocation(shaderProgram, "cameraPos");
		glUniform3f(cameraPosUniform, x, y, z);

		GLuint cameraRotUniform = glGetUniformLocation(shaderProgram, "cameraRot");
		glUniform2f(cameraRotUniform, yaw, pitch);

		// Reset timer
		glfwSetTime(0.0);

		// Clear and draw
		glClear(GL_COLOR_BUFFER_BIT);
		glDrawArrays(GL_TRIANGLES, 0, 6);

		// Swap buffers
		glfwSwapBuffers();
		
	}
	while(glfwGetKey(GLFW_KEY_ESC) != GLFW_PRESS && glfwGetWindowParam(GLFW_OPENED));


	glDeleteProgram(shaderProgram);
	glDeleteShader(fragmentShader);
	glDeleteShader(vertexShader);

	glDeleteBuffers(1, &vbo);

	glDeleteVertexArrays(1, &vao);


	glfwTerminate();
	return 0;
}
